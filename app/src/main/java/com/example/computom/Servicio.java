package com.example.computom;


import android.app.Service;
import android.content.Context;
import android.content.Intent;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.IBinder;
import android.support.annotation.Nullable;


public class Servicio extends Service {


    private Context context;

    // Variables del Sensor
    SensorManager sensorManagerS;
    Sensor sensorS;
    SensorEventListener sensorEventListener;
    int whip=0;


    private static final float mov_min = 5;
    private static final double vectorMag_min = 8.778505249186788;
    private float curX = 0, curY = 0, curZ = 0;

    public Servicio(){
        super();
        this.context = this.getApplicationContext();

    }

    public Servicio(Context c){
        super();
        this.context=c;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
    public void activaServicio(SensorManager sensorManager, Sensor sensor, final SnapshotActivity snapshotActivity){


        sensorManagerS = sensorManager;
        sensorS = sensor;


        sensorEventListener=new SensorEventListener() {
            @Override
            public void onSensorChanged(SensorEvent event) {
                synchronized (this) {
                    float suma=0;
                    curX = event.values[0];
                    curY = event.values[1];
                    curZ = event.values[2];

                    float movimiento =0;
                    System.out.println("Valor del Giro: " + curX +", " + curY+ ", " + curZ);

                    movimiento = Math.abs((curX+curY+curZ)/3);

                    /*
                     Calculo del vector magnitud

                     Calcula la distancia entre un punto A a un punto B
                     */

                    //double X2=-0.2881 , X1=-0.2977, Y2=8.8463, Y1=0.0678;
                    double  vectorMag = Math.abs(Math.sqrt(Math.pow(((curX)-(-0.2977)),2)+Math.pow(((curY)-(0.0678)),2)));
                    System.out.println("Vector mag.... " + vectorMag);

                    //System.out.println("Promedio: "+movimiento);

                    if(vectorMag >  vectorMag_min){
                         snapshotActivity.getWeather();
                        //whip++;
                    }
                }
            }

            @Override
            public void onAccuracyChanged(Sensor sensor, int accuracy) {

            }
        };
        start();

    }

    private void start(){
        sensorManagerS.registerListener(sensorEventListener, sensorS, SensorManager.SENSOR_DELAY_UI);

    }
    private void stop(){
        sensorManagerS.unregisterListener(sensorEventListener);
    }


}
