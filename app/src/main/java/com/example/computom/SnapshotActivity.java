package com.example.computom;

import android.Manifest;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Location;
import android.media.MediaPlayer;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.awareness.Awareness;
import com.google.android.gms.awareness.snapshot.LocationResult;
import com.google.android.gms.awareness.snapshot.WeatherResult;
import com.google.android.gms.awareness.state.Weather;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class SnapshotActivity extends AppCompatActivity {

    // Variables de la Awareness API
    private GoogleApiClient mGoogleApiClient;
    private static final String TAG = "MainActivity";
    private static final int MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 10001;

    // Variables del diseño
    private Button mSnapshotButton;
    private TextView mWeatherTextView;
    private TextView mTimeTextView;
    private TextView info_text2;
    private ImageView imgImagen;
    private TextView datosExtras;
    // Variables del Sensor
    SensorManager sensorManager;
    Sensor sensor;
    SensorEventListener sensorEventListener;

    Servicio servicio;
    // Variables para Notificar
    private PendingIntent pendingIntent;
    private final static String CHANNEL_ID = "NOTIFICACION";
    private final static int NOTIFICACION_ID = 0;

    String tip="";
    int imagenTip=0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_snapshot);

        servicio = new Servicio(getApplicationContext());

        sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        sensor = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);


        servicio.activaServicio(sensorManager, sensor, SnapshotActivity.this);


        mGoogleApiClient = new GoogleApiClient.Builder(SnapshotActivity.this)
                .addApi(Awareness.API)
                .build();
        mGoogleApiClient.connect();

        mWeatherTextView = (TextView) findViewById(R.id.weatherTextView);
        mTimeTextView = (TextView) findViewById(R.id.mTime);
        info_text2= (TextView) findViewById(R.id.info_text2);
        imgImagen = (ImageView) findViewById(R.id.imgClima);
        datosExtras = (TextView) findViewById(R.id.mDatosExt);

        getWeather();

       /* mSnapshotButton = (Button) findViewById(R.id.snapshotButton);
        mSnapshotButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                  getWeather();

                 // setPendingIntent();

            }
        });*/

    }

    public int validaImagen(int temperatura) {

        int respuesta=0;

        if (temperatura >= 20 & temperatura < 30){
            respuesta = R.drawable.nublado;

        }else if (temperatura >= 30 & temperatura <= 35){
            respuesta= R.drawable.sol;


        }else if (temperatura >= 10 & temperatura < 20 ){
            respuesta=R.drawable.nieve;

        }else{
            respuesta=R.drawable.termometro;
        }
        return  respuesta;
    }

    public String validaciones(int temperatura) {

        String respuesta="";

        if (temperatura >= 20 & temperatura < 30){
         respuesta = "La tarde está soleada y agradable, deberías vestir ropa casual";

        }else if (temperatura >= 30 & temperatura <= 35){
            respuesta= "La tarde está soleada, deberías vestir ropa casual";


        }else if (temperatura >= 10 & temperatura < 20 ){
            respuesta="Esta frío, deberías abrigarte\n";

        }else{
            respuesta="No hay recomendaciones disponibles";
        }
       return  respuesta;
    }

    public void getWeather() {


        Calendar calendar = Calendar.getInstance();
        mTimeTextView.setTextColor(Color.WHITE);
        mTimeTextView.setText(new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").format(calendar.getTime()));

        // Check for permission first
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            Log.e(TAG, "Fine Location Permission not yet granted");

            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);

        } else {
            System.out.println("else of getWeather()");
            Log.i(TAG, "Fine Location permission already granted");

            // Clima
            Awareness.SnapshotApi.getWeather(mGoogleApiClient)
                    .setResultCallback(new ResultCallback<WeatherResult>() {
                        @Override
                        public void onResult(@NonNull WeatherResult weatherResult) {
                            if (!weatherResult.getStatus().isSuccess()) {
                                Log.e(TAG, "No se ha detectado información del clima");
                                mWeatherTextView.setText("No se ha detectado información del clima");
                                mWeatherTextView.setTextColor(Color.RED);
                                System.out.println("No se ha detectado información del clima");
                                return;
                            }
                            System.out.println("Información del Clima");
                            Weather weather = weatherResult.getWeather();
                            System.out.println("Todo: "+weather);
                            int temp = (int)weather.getTemperature(Weather.CELSIUS);

                            mWeatherTextView.setTextColor(Color.rgb(236,53,0));
                            mWeatherTextView.setTextColor(Color.WHITE);
                            Log.e(TAG, "Temperatura: " + temp);
                            mWeatherTextView.setText(temp+"°C");

                            datosExtras.setTextColor(Color.WHITE);
                            datosExtras.setText("Humedad: "+String.valueOf(weather.getHumidity()+"%"));

                            // Este método obtiene la recomendación según la temputatura del clima
                            tip=validaciones(temp);
                            imagenTip = validaImagen(temp);

                            // Se muestra la recomendacion del clima
                            info_text2.setTextColor(Color.WHITE);
                            info_text2.setText(tip);


                            imgImagen.setImageResource(imagenTip);



                            // Se encarga de mostrarle la recomentación al usuario en la notificaciones
                            createNoficationChannel();
                            createNotificacion(String.valueOf(temp), tip);
                           /* for (int i = 0; i < weather.getConditions().length; i++) {
                                Log.e(TAG, "Temperatura: " + weather.getConditions()[i]);
                            }*/
                        }
                    });


        }
    }

    private void setPendingIntent(){
       Intent intent = new Intent(this, SnapshotActivity.class);
       //pendingIntent

    }
    private void createNoficationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = "Notificacion";
            NotificationChannel notificationChannel = new NotificationChannel(CHANNEL_ID, name, NotificationManager.IMPORTANCE_DEFAULT);
            NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
            notificationManager.createNotificationChannel(notificationChannel);
        }
    }

    private void createNotificacion(String temp, String tip) {
        NotificationCompat.Builder builder = new NotificationCompat.Builder(getApplicationContext(), CHANNEL_ID);
        builder.setSmallIcon(R.drawable.ic_cloud_black_24dp);
        builder.setContentTitle("Temperatura actual de: "+temp+ "°C");
        builder.setContentText(tip);
        builder.setColor(Color.rgb(19,130,184));
        builder.setPriority(NotificationCompat.PRIORITY_DEFAULT);
        builder.setLights(Color.MAGENTA, 1000, 1000);
        builder.setVibrate(new long[]{1000});
        builder.setDefaults(Notification.DEFAULT_SOUND);

        builder.setContentIntent(pendingIntent);

        NotificationManagerCompat notificationManagerCompat = NotificationManagerCompat.from(getApplicationContext());
        notificationManagerCompat.notify(NOTIFICACION_ID, builder.build());

    }

}
