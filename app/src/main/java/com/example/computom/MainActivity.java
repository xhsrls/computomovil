package com.example.computom;

import android.Manifest;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.google.android.gms.awareness.Awareness;
import com.google.android.gms.awareness.snapshot.WeatherResult;
import com.google.android.gms.awareness.state.Weather;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;

public class MainActivity extends AppCompatActivity {

    private GoogleApiClient mGoogleApiClient;

    private Button mSnapshotButton;
    private Button mFenceButton;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


       mSnapshotButton = (Button) findViewById(R.id.snapshotButton);mSnapshotButton.setOnClickListener(new View.OnClickListener() {
        @Override
           public void onClick(View view) {
             startActivity(new Intent(MainActivity.this, SnapshotActivity.class));
           }
        });

        startActivity(new Intent(MainActivity.this, SnapshotActivity.class));
    }
}
